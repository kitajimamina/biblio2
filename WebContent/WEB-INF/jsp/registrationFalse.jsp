<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登録に失敗しました</title>
</head>
<body>
<h1>登録に失敗しました</h1>
<p>申し訳ありませんがもう一度入力してください。</p>
<a href = "registrationForm.jsp">戻る</a>
</body>
</html>