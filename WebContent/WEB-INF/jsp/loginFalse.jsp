<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="model.user"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ログインに失敗しました</title>
</head>
<body>
	<h1>ログインに失敗しました</h1>
	<p>申し訳ありませんが、もう一度ID、パスワードを入力してください。</p>
	<a href="/Biblio/">ログイン画面へ</a>
</body>
</html>