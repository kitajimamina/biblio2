package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Create;

public class CreateDAO {

	// データベース接続に使用する情報
	private final String JDBC_URL = "jdbc:h2:tcp://localhost/~/Biblio";
	private final String DB_USER = "sa";
	private final String DB_PASS = "";

	public List<Create> findAll() {
		List<Create> createList = new ArrayList<>();

		// データベース接続
		try (Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)) {
			// SELECT文の準備
			String sql = "SELECT id,title,name,text FROM Create ORDER BY ID DESC";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			// SELECTを実行
			ResultSet rs = pStmt.executeQuery();
			
			//SELECT文の結果をArrayListに格納
			while(rs.next()){
				int id = rs.getInt("id");
				String title = rs.getString("title");
				String name = rs.getString("name");
				String text = rs.getString("text");
				Create create = new Create(id,title,name,text);
				createList.add(create);
			}
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return null;
		}
		return createList;

	}
	
	public boolean create(Create create){
		//データベース接続
		try(Connection conn = DriverManager.getConnection(JDBC_URL,DB_USER,DB_PASS)){
			//INSERT文の準備(idは自動連番なので指定しなくてよい)
			String sql = "INSERT INTO Create(title,name,text) VALUES(?,?,?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			
			//INSERT文中の「?」に使用する値を設定しSQLを完成
			pStmt.setString(1, create.getTitle());
			pStmt.setString(2, create.getName());
			pStmt.setString(3, create.getText());
			
			//INSERT文を実行(resultには追加された行数が代入される)
			int result = pStmt.executeUpdate();
			if(result != 1){
				return false;
			}
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
