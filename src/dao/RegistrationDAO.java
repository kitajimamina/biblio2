package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.Account;

public class RegistrationDAO {

	// データベースに接続に使用する情報
	private final String JDBC_URL = "jdbc:h2:tcp://localhost/~/Biblio";
	private final String DB_USER = "sa";
	private final String DB_PASS = "";

	public boolean join(Account account) {
		// データベース接続
		try (Connection conn = DriverManager.getConnection(JDBC_URL, DB_USER, DB_PASS)) {

			// INSERT文の準備(idは自動連番なので指定しなくてもよい)
			String sql = "INSERT INTO Account(password,mail,name) VALUES(?,?,?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);

			// INSERT文中の「?」に使用する値を設定し、SQLを完成
			pStmt.setString(1, Account.getPassword());
			pStmt.setString(2, Account.getMail());
			pStmt.setString(3, Account.getName());

			// INSERT文を実行 (resultには追加された行数が代入される)
			int result = pStmt.executeUpdate();
			if (result != 1) {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
