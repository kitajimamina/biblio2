package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Account;
import model.RegistrationLogic;

@WebServlet("/Registration")
public class Registration extends HttpServlet{

	private static final long serialversionUID = 1L;
	protected void doPost(HttpServletRequest request,HttpServletResponse response)
	throws ServletException, IOException{
		
		//リクエストパラメータの取得
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String mail = request.getParameter("mail");
		String name = request.getParameter("name");
		
		//Accountインスタンス (インスタンス情報)の生成
		Account account = new Account(id, password, mail, name);
		
		//登録処理
		RegistrationLogic RegistrationLogic = new RegistrationLogic();
		boolean isRegistration = RegistrationLogic.execute(account);
		
		//登録成功時の処理
		if(isRegistration){
			//作品一覧画面へフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/main.jsp");
			dispatcher.forward(request, response);
		}else{ //登録失敗時の処理
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/registrationFalse.jsp");
			dispatcher.forward(request, response);
		}
	}
}
