package model;

public class Account {

	private String id;
	private static String password;
	private static String mail;
	private static String name;

	public Account() {

	}

	public Account(String id, String password, String mail, String name) {
		this.id = id;
		this.password = password;
		this.mail = mail;
		this.name = name;
	}
	// ゲッター・セッター自動生成

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public static String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public static String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
