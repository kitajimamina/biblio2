package model;

import dao.RegistrationDAO;

public class RegistrationLogic {

	int passkazu = Account.getPassword().length();
	
	public boolean execute(Account account){
		if (passkazu >= 4){
			RegistrationDAO dao = new RegistrationDAO();
			dao.join(account);
			return true;
		}
		return false;
	}
}
