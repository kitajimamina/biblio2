package model;

import java.util.List;

import dao.CreateDAO;

public class GetCreateListLogic {

	public List<Create> execute() {
		CreateDAO dao = new CreateDAO();
		List<Create> createList = dao.findAll();
		return createList;
	}
}
