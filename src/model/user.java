package model;

public class user {

	private String id; // ユーザーID
	private String password; // パスワード

	public user() {

	}

	public user(String id, String password) {
		this.id = id;
		this.password = password;
	}

	// ゲッター・セッター自動生成

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
