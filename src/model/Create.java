package model;

import java.io.Serializable;

public class Create implements Serializable{

	private int createId;
	private String title;
	private String name;
	private String text;
	
	public Create(){
		
	}
	
	public Create(int createId,String title,String name,String text){
		this.createId = createId;
		this.title = title;
		this.name = name;
		this.text = text;
	}

	public int getCreateId() {
		return createId;
	}

	public String getTitle() {
		return title;
	}

	public String getName() {
		return name;
	}

	public String getText() {
		return text;
	}
	
	
}
